#### Setup environment with new features?

This tool provides used to install and test on UI web application, etc. The application installed on java 8 SDK.

## Installation Steps
1. Install java 8 SDK and set JAVA_HOME environment variable on operating system.

```bash
 ex: set JAVA_HOME "C:\Program Files\Java\jdk1.8.0_202\bin"
```

2. Clone the repositor
```bash
git clone https://gitlab.com/nhattuyen115/tinypulse.git
```

3. Change the working directory

```bash
cd tinypulse
```

4. Install dependencies

Depending on the IDE used, it may automatically download packages from the maven repository. If the IDE does not download itself, please open the pom.xml file, then download the correct version from the maven web repository: 


[Maven](https://mvnrepository.com/)

5. Download driver

The application is run on the chrome browser. It used chrome driver browser. You can go to the website [chromedriver](https://chromedriver.chromium.org/downloads) and download the browser version compatible driver on your computer.
Then put it to drivers folder of project.

## Run

Open test cases on IDE and press play button. can run all test cases or test cases one by one