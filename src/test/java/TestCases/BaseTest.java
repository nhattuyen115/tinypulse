package TestCases;

import Driver.DriverManager;
import Driver.DriverManagerFactory;
import Enum.DriverType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    DriverManager driverManager;
    protected WebDriver driver;

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {
//        driverManager = DriverManagerFactory.getManager(DriverType.CHROME);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        driverManager = DriverManagerFactory.getManager(DriverType.CHROME);
        driver = driverManager.getDriver();
    }

    @AfterMethod
    public void afterMethod() {
        driverManager.quitDriver();
    }
}
