package TestCases;

import PageObjects.InviteFinishPage;
import PageObjects.AddEmployeePage;
import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import Util.Common;
import org.testng.annotations.Test;

public class AddEmployeeTest extends BaseTest {

    @Test
    public void addNewPeople() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigateToLoginPage();
        loginPage.submitLogin("nhattuyen115@yahoo.com", "Aa@12345");

        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.navigateToDashboardPage();
        dashboardPage.clickInviteUser();

        AddEmployeePage employeePage = new AddEmployeePage(driver);
        String sfirstName = "Employee";
        String slastName = "E";
        String sEmail = Common.generatingRandomString() + "@yopmail.com";

        employeePage.clickAddPeople(sfirstName, slastName, sEmail);

        InviteFinishPage finishPage = new InviteFinishPage(driver);
        finishPage.verifySuccess();
    }

    @Test
    public void addPeopleExistedEmail() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigateToLoginPage();
        loginPage.submitLogin("nhattuyen115@yahoo.com", "Aa@12345");

        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.navigateToDashboardPage();
        dashboardPage.clickInviteUser();

        AddEmployeePage employeePage = new AddEmployeePage(driver);
        String sfirstName = "Employee";
        String slastName = "E";
        String sEmail = "ab1@yopmail.com";

        employeePage.clickAddPeople(sfirstName, slastName, sEmail);

        InviteFinishPage finishPage = new InviteFinishPage(driver);
        finishPage.verifyNotifyErrorEmailExist();
    }
}
