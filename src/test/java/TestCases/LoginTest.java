package TestCases;

import PageObjects.LoginPage;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test
    public void verifyLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigateToLoginPage();
        loginPage.submitLogin("nhattuyen115@yahoo.com", "Aa@12345");
    }
}
