package TestCases;

import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import org.testng.annotations.Test;

public class DashboardTest extends BaseTest {

    @Test
    public void verifyDashboard() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigateToLoginPage();
        loginPage.submitLogin("nhattuyen115@yahoo.com", "Aa@12345");

        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.navigateToDashboardPage();
        dashboardPage.clickInviteUser();
    }
}
