package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BasePage {
    WebDriver driver;
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public BasePage(WebDriver driver) {
        this.driver = driver;

        // This init Elements method will create all WebElements

        PageFactory.initElements(driver, this);
    }
}
