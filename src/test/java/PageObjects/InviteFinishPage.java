package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class InviteFinishPage extends BasePage {
    @FindBy(xpath = "//div[@class='tu mv3 fw6 f3 flex items-center']")
    WebElement notifyResult;

    public InviteFinishPage(WebDriver driver) {
        super(driver);
    }

    public void verifySuccess() {
        String current_url = driver.getCurrentUrl();

        String notify = driver.findElement(By.xpath("//div[contains(@class, 'tu mv3 fw6 f3 flex items-center')]")).getText();
        notify = notify.replaceAll("\\<[^>]*>","");

        Assert.assertEquals("check_circle\nCongratulations", notify);
    }

    public void verifyNotifyErrorEmailExist() {
        String current_url = driver.getCurrentUrl();

        String notify = driver.findElement(By.xpath("//div[contains(@class, 'tu mv3 fw6 f3 flex items-center')]")).getText();
        notify = notify.replaceAll("\\<[^>]*>","");

        Assert.assertEquals("error\nUh oh! Unable to add user because email already exists", notify);
    }
}
