package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AddEmployeePage extends BasePage {
    @FindBy(id = "tp-container")
    WebElement container;

    @FindBy(xpath = "//input[@field='firstName' and @refkey='1']")
    WebElement firstName;

    @FindBy(xpath = "//input[@field='lastName' and @refkey='1']")
    WebElement lastName;

    @FindBy(xpath = "//input[@field='email' and @refkey='1']")
    WebElement email;

    @FindBy(xpath = "//div[contains(@class, 'cucumber-send-invite-button')]")
    WebElement buttonAdd;

    public AddEmployeePage(WebDriver driver) {
        super(driver);
    }

    public void clickAddPeople(String sfirstName, String slastName, String sEmail) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(container));

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        firstName.clear();
        firstName.sendKeys(sfirstName);

        lastName.clear();
        lastName.sendKeys(slastName);

        email.clear();
        email.sendKeys(sEmail);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String current_url = driver.getCurrentUrl();
        buttonAdd.click();

        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains("invite/finish"));
    }
}
