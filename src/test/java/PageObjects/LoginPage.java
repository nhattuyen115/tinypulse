package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import Data.GlobalVariable;
import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {
    protected String pageURL = GlobalVariable.BASE_ENV_URL + "signin";

    static final String LOG_IN_MSG = "Logged in as {}";

    @FindBy(xpath = "//input[@data-cucumber='input-login-email']")
    WebElement emailField;

    @FindBy(xpath = "//input[@data-cucumber='input-login-password']")
    WebElement passwordField;

    @FindBy(xpath = "//div[@data-cucumber='button-continue']")
    WebElement btnContinue;

    @FindBy(xpath = "//div[@data-cucumber='button-login']")
    WebElement btnLogin;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void navigateToLoginPage() {
        driver.get(pageURL);
    }

    public void submitLogin(String username, String password) {
        emailField.clear();
        emailField.sendKeys(username);
        btnContinue.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        passwordField.clear();
        passwordField.sendKeys(password);
        btnLogin.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LOGGER.info(LOG_IN_MSG, username);
    }
}
