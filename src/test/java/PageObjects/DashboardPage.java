package PageObjects;

import Data.GlobalVariable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class DashboardPage extends BasePage {
    protected String pageURL = GlobalVariable.BASE_ENV_URL + "home/engage/dashboard";

    @FindBy(xpath = "//div[@data-cucumber='sitebar-menu-items']")
    WebElement siteBar;

    @FindBy(xpath = "//li[@data-name='settings']")
    WebElement settingSidebar;

    @FindBy(xpath = "//span[contains(text(), 'Add People')]")
    WebElement settingUserInvite;

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public void navigateToDashboardPage() {
        driver.get(pageURL);
    }

    public void clickInviteUser() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(settingSidebar)).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        settingUserInvite.click();
    }
}
