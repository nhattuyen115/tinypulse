package PageObjects;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import Data.GlobalVariable;

public class SignupPage extends BasePage {
    protected String pageURL = GlobalVariable.BASE_ENV_URL + "engage/signup";

    @FindBy(id = "inputEmail")
    WebElement emailField;

    @FindBy(className = "btn-start-trial")
    WebElement btnNext;

    public SignupPage(WebDriver driver) {
        super(driver);
    }

    public void navigateToSignupPage() {
        driver.get(pageURL);
//        LOGGER.info("Loading page Signup");
//        HomePage homePage = signInPage.loginValidUser("userName", "password");
//        assertThat(homePage.getMessageText(), is("Hello userName"));
    }
}
