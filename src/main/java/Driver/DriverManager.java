package Driver;

import org.openqa.selenium.WebDriver;
import Data.GlobalVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DriverManager {

    protected WebDriver driver;
    protected abstract void startService();
    protected abstract void stopService();
    protected abstract WebDriver createDriver();
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public void quitDriver() {
        if (null != driver) {
            try {
                if (GlobalVariable.closeBrowser == true) {
                    stopService();
                    driver.quit();
                    driver = null;
                }
            } catch (Exception e) {
                LOGGER.error("Driver: driver is not ready in this time");
            }
        }
    }

    public WebDriver getDriver() {
        if (null == driver) {
            startService();
            createDriver();
        }

        return driver;
    }
}
