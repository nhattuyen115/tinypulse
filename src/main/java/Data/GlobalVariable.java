package Data;

public class GlobalVariable {
    public static final String BASE_ENV_URL = getEvnBaseURL();
    public static final Boolean closeBrowser = true;
    static final String RETURN_DOC_STATE =
            "if (document && document.readyState) return document.readyState; else return 'Browser does not load';";

    public static String getEvnBaseURL() {
        String url = "https://staging.tinyserver.info/";
        System.out.println(">>>>>> (Temporary for local ) > Evn base URL is : " + url);
        return url;
    }
}
